import React from 'react';
import { connect } from 'umi';
import {Spin, Row, Col} from 'antd';
import CorrelateListTree from "../../components/user/allotAuthorize/CorrelateListTree";
import AllotAuthorizeSearch from "../../components/user/allotAuthorize/AllotAuthorizeSearch";
import AllotAuthorizeToolbar from "../../components/user/allotAuthorize/AllotAuthorizeToolbar";
import AllotAuthorizeList from "../../components/user/allotAuthorize/AllotdAuthorizeList";
import TablePagination from '../../components/common/TablePagination';
import AllotAuthorizeModal from "../../components/user/allotAuthorize/AllotAuthorizeModal";
import MenuLimitDrawer from "../../components/user/allotAuthorize/MenuLimitDrawer";
import UserAllotTransfer from "../../components/user/allotAuthorize/UserAllotTransfer";
import ViewAuthorizeDrawer from "../../components/user/allotAuthorize/ViewAuthorizeDrawer";

class AllotAuthorizePage extends React.Component {

  render() {

    let {dispatch, globalModel, allotAuthorizeModel} = this.props;
    const { authorizeData, total, currentPage, pageSize, listLoading, operateType, departmentModalVisible,
      allUserList, selectedRowKeys, selectedRows, departmentInfoData, searchParams, radioValue, allotId,
      correlateList, menuLimitDrawerVisible, userAllotTransferVisible, menuList, correlateRecord,
      viewAuthorizeDrawerVisible, authorizeDetailRecord, menuDrawerType, correlateSelectedKeys } = allotAuthorizeModel;

    const correlateAuthorizeSearchProps = {
      radioValue,
      onSearch: (searchParams) => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {searchParams}});
        dispatch({type: 'allotAuthorizeModel/getListDatas', payload: {currentPage, pageSize, params: searchParams}});
      },
      onReset: () => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {searchParams: null}});
      },
      onShowMenuDrawer: () => {
        dispatch({type: "allotAuthorizeModel/getMenuList", payload: {}});
        dispatch({type: "allotAuthorizeModel/updateState", payload: {menuLimitDrawerVisible: true, menuDrawerType: "search"}});
      }
    }

    const correlateAuthorizeModalProps = {
      operateType,
      departmentModalVisible,
      departmentInfoData,
      onSave: (values) => {
        values.parentId = values.parentId ? values.parentId : "";
        values.longCode = values.longCode ? values.longCode : "";
        if (values.type == "2" || values.type == "3") {
          const value = selectedRows[0];
          values.parentId = value.id;
          values.longCode = value.longCode;
        }
        dispatch({type: "allotAuthorizeModel/addDepartment", payload: values});
      },
      updateDepartment: (values) => {
        dispatch({type: "allotAuthorizeModel/updateDepartment", payload: values});
      },
      onCancel: () => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {departmentModalVisible: false}});
      },
    }

    const correlateAuthorizeToolbarProps = {
      radioValue,
      onAllot: () => {
        Promise.all([dispatch({type: "allotAuthorizeModel/getAllUserList", payload: {}})]).then(res =>
          dispatch({type: "allotAuthorizeModel/updateState", payload: {userAllotTransferVisible: true}})
        );
      },
      onAuthorize: () => {
        dispatch({type: "allotAuthorizeModel/getMenuList", payload: {}});
        dispatch({type: "allotAuthorizeModel/updateState", payload: {menuLimitDrawerVisible: true}});
      },
    }

    const correlateListTreeProps = {
      currentPage,
      pageSize,
      authorizeData,
      listLoading,
      correlateList,
      radioValue,
      correlateRecord,
      correlateSelectedKeys,
      onRadioChange: (e) => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {radioValue: e.target.value}});
        dispatch({type: "allotAuthorizeModel/getListDatas", payload: {radioValue: e.target.value}});
      },
      onSelectTree: (selectedKeys, item) => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {allotId: selectedKeys[0], correlateRecord: item.node.dataRef}});
        dispatch({type: "allotAuthorizeModel/getAllotAuthorizeData", payload: {allotId: selectedKeys[0], radioValue}});
      },
    }

    const correlateAuthorizeListProps = {
      currentPage,
      pageSize,
      authorizeData,
      listLoading,
      correlateList,
      radioValue,
      onDelete: (record) => {
        Promise.all([dispatch({type: "allotAuthorizeModel/deleteAllots", payload: {ids: record.id}})]).then(res =>
          dispatch({type: "allotAuthorizeModel/getAllotAuthorizeData", payload: {allotId: correlateRecord.id, radioValue}})
        );
      },
      onViewDetail: (record) => {
        dispatch({
          type: "allotAuthorizeModel/getAllotAuthorizeDetail",
          payload: {allotId: record.allotId, correlateId: record.correlateId, type: radioValue}
        });
        dispatch({type: "allotAuthorizeModel/updateState", payload: {viewAuthorizeDrawerVisible: true}});
      },
    }

    const tablePaginationProps = {
      total,
      currentPage,
      pageSize,
      onPageChange: (currentPage, pageSize) => {
        dispatch({type: 'allotAuthorizeModel/getListDatas', payload: {currentPage, pageSize, ...searchParams}});
      },
      onShowSizeChange: (currentPage, pageSize) => {
        dispatch({type: 'allotAuthorizeModel/getListDatas', payload: {currentPage, pageSize, ...searchParams}});
      },
    }

    const menuLimitDrawerProps = {
      menuList,
      menuLimitDrawerVisible,
      authorizeData,
      onClose:() => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {menuLimitDrawerVisible: false}});
      },
      onSelectTreeNode: (checkedTreeNodeKeys) => {
        if ("authorize" == menuDrawerType) {
          const selectedKeys = checkedTreeNodeKeys.join(",");
          const type = "post" == radioValue ? "3" : "5";
          dispatch({type: "allotAuthorizeModel/onAllotCorrelate", payload: {allotId, selectedKeys, type}});
        }
        dispatch({type: "allotAuthorizeModel/updateState", payload: {menuLimitDrawerVisible: false}});
      }
    }

    const userAllotTransferProps = {
      allUserList,
      authorizeData,
      userAllotTransferVisible,
      onCancel: () => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {userAllotTransferVisible: false}});
      },
      onAllotUser: (selectedKeys) => {
        selectedKeys = selectedKeys.join(",");
        const type = "department" == radioValue ? "1" : "post" == radioValue ? "2" : "4";
        dispatch({type: "allotAuthorizeModel/onAllotCorrelate", payload: {allotId, selectedKeys, type}});
        dispatch({type: "allotAuthorizeModel/updateState", payload: {userAllotTransferVisible: false}});
      },
    }

    const viewAuthorizeDrawerProps = {
      radioValue,
      viewAuthorizeDrawerVisible,
      authorizeDetailRecord,
      onClose:() => {
        dispatch({type: "allotAuthorizeModel/updateState", payload: {viewAuthorizeDrawerVisible: false}});
      },
    }

    return (
      <div style={{height: "100%", width: "100%"}}>
        <Spin spinning={listLoading}>
          <Row style={{height: "100%", width: "100%"}}>
            <Col span={5} style={{height: "100%", width: "100%", padding: "0px 18px 0px 0px", borderRight: "1px solid #f0f0f0"}}>
              <CorrelateListTree {...correlateListTreeProps} />
            </Col>
            <Col span={19} style={{height: "100%", width: "100%", padding: "0px 0px 0px 20px"}}>
              <AllotAuthorizeSearch {...correlateAuthorizeSearchProps} />
              <AllotAuthorizeToolbar {...correlateAuthorizeToolbarProps} />
              <AllotAuthorizeList {...correlateAuthorizeListProps} />
              <TablePagination {...tablePaginationProps} />
              <AllotAuthorizeModal {...correlateAuthorizeModalProps} />
              <MenuLimitDrawer {...menuLimitDrawerProps} />
              <UserAllotTransfer {...userAllotTransferProps} />
              <ViewAuthorizeDrawer {...viewAuthorizeDrawerProps} />
            </Col>
          </Row>
        </Spin>
      </div>
    );
  };
}

function mapStateToProps({globalModel, allotAuthorizeModel}){
  return {globalModel, allotAuthorizeModel};
}

export default connect(mapStateToProps)(AllotAuthorizePage);
