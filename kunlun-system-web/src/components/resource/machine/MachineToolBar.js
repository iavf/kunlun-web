import React from 'react';
import { Button, Icon } from 'antd';

const MachineToolBar = (props) => {

  const { onRefresh, onExport } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    marginRight: "5px",
  }

  return (
    <div style={{ marginBottom: "15px", marginTop: "15px" }}>
      <Button type="primary" size="default" icon={<i className="ri-refresh-line" style={iconStyle}></i>} onClick={onRefresh}>刷新</Button>
      <Button type="dashed" size="default" icon={<i className="ri-download-2-line" style={iconStyle}></i>} style={{ marginLeft: "15px" }} onClick={onExport}>导出</Button>
    </div>
  );
};

export default MachineToolBar;
