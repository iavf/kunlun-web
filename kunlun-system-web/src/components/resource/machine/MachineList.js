import React from 'react';
import { Table, Tooltip } from 'antd';
import config from '../../../config/config';
import styles from './Machine.less';

const MachineList = (props) => {

  const { machineList, onDetail, currentPage, pageSize } = props;

  const iconStyle = {
    verticalAlign: "bottom",
    fontSize: "18px",
  }

  const columns = [
    { title: '序号', key: '', width: '5%', align: "center",
      render: (text, record, index) => (index + 1) + (currentPage - 1) * pageSize },
    { title: '服务名', dataIndex: 'serviceName', key: 'serviceName', width: '15%', align: "center" },
    { title: 'ip地址', dataIndex: 'ipAddress', key: 'ipAddress', width: '11%', align: "center" },
    { title: 'CPU', dataIndex: 'totalCPU', key: 'totalCPU', width: '8%', align: "center",
      render: (text, record, index) => (parseInt(record.cpuLogicalProcessorCount) + parseInt(record.cpuPhysicalProcessorCount)) + "/" + record.cpuUsedPercent },
    { title: '内存', dataIndex: 'totalMomery', key: 'totalMomery', width: '12%', align: "center",
      render: (text, record, index) => record.memoryTotal + "/" + record.memoryUsed },
    { title: '磁盘', dataIndex: 'totalMomery', key: 'totalMomery', width: '10%', align: "center",
      render: (text, record, index) => record.diskTotalSize + "/" + record.diskUsedSize },
    { title: 'JVM内存', dataIndex: 'totalMomery', key: 'totalMomery', width: '13%', align: "center",
      render: (text, record, index) => record.jvmUsedMemory + "/" + record.jvmTotalMemory },
    { title: '每秒接收/发送KB数', dataIndex: 'totalMomery', key: 'totalMomery', width: '13%', align: "center",
      render: (text, record, index) => record.netReceiveKbCount + "/" + record.netSendKbCount },
    { title: '操作', key: 'operate', width: '8%', align: "center", render: (text, record, index) => (
      <span>
        <a onClick={() => onDetail(record)}>
          <Tooltip title={"查看详情"}>
            <i className="ri-file-text-line" style={iconStyle}></i>
          </Tooltip>
        </a>
      </span>)
    }];

  return (
    <div className={ styles.menuTable }>
      <Table
        size={"small"}
        columns={columns}
        dataSource={machineList}
        bordered={true}
        rowKey={record => record.id}
        pagination={false}
        scroll={{y: (window.innerHeight - 195)}}
      />
    </div>
  );
};

export default MachineList;
