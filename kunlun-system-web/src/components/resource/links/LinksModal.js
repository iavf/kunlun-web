import React from 'react';
import { Modal, Form, Input, Row, Col, Select, Icon } from 'antd';
import config from '../../../config/config';
import styles from './Links.less';

const FormItem = Form.Item;
const SelectOption = Select.Option;

class LinksModal extends React.Component {

  constructor(props) {
    super(props);
  }

  formRef = React.createRef();

  render() {

    // 从传递过来的props中获取参数
    const {
      linksModalVisible, onCancel, onSave, operateType, saveLoading, linkRecord, linkType
    } = this.props;

    const formItemLayout = {
      labelCol: {span: 8},
      wrapperCol: {span: 16},
    }
    const singleFormItemLayout = {
      labelCol: {span: 4},
      wrapperCol: {span: 20},
    }

    // 点击Modal框确定按钮触发的事件
    const onOk = () => {

      debugger

      this.formRef.current.validateFields((err, values) => {
        if (!err) {
          onSave(values);
        }
      });
    }

    const linkTypes = [{ key: "system", name: "系统链接" }, { key: "custom", name: "外部链接" }];
    const linksTypeOptions = linkTypes.map(item => <SelectOption key={item.key} value={item.key}>{item.name}</SelectOption>);

    // 返回工具栏新增、批量删除按钮
    return (
      <div>
        <Modal
          centered={true}
          className={styles.modal}
          visible={linksModalVisible}
          title={operateType == "add" ? "新增菜单" : "编辑菜单"}
          okText="保存"
          onCancel={onCancel}
          onOk={onOk}
          width={650}
          destroyOnClose={true}
        >
          <Form style={{marginLeft: "-4%"}} ref={this.formRef}>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="链接名" name={"name"} rules={[{required: false, message: '请输入链接名'}]}>
                  <Input/>
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="链接图标" name={"icon"} rules={[{required: false, message: '请选择链接图标'}]}>
                  <Input/>
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label="链接类型" name={"type"} rules={[{required: false, message: '请选择链接类型'}]}>
                  <Select>{ linksTypeOptions }</Select>
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label="图标颜色" name={"color"} rules={[{required: false, message: '请选择图标颜色'}]}>
                  <Input />
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <FormItem {...singleFormItemLayout} label="链接URL" name={"url"} rules={[{required: false, message: '请输入链接URL'}]}>
                  <Input suffix={<Icon type="down" className="certain-category-icon"/>}/>
                </FormItem>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <FormItem {...singleFormItemLayout} label="备注" name={"remark"} rules={[{required: false, message: '请输入备注'}]}>
                  <Input.TextArea />
                </FormItem>
              </Col>
            </Row>
          </Form>
        </Modal>
      </div>
    );
  };
}

export default LinksModal;
