import React from 'react';
import { Modal } from 'antd';
import styles from './Picture.less';

class ViewModal extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {

    const {
      viewModalVisible, onClose, pictureRecord
    } = this.props;

    return (
      <div>
        <Modal
          centered={true}
          className={styles.modal}
          visible={viewModalVisible}
          title={"查看图片"}
          onCancel={onClose}
          width={450}
          destroyOnClose={true}
          footer={[]}
        >
          <img width={400} height={360} src={ "data:" + pictureRecord.type + ";base64," + pictureRecord.base64 } />
        </Modal>
      </div>
    );
  };
}

export default ViewModal;
