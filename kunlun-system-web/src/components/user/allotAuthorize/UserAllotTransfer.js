import React from 'react';
import { Modal, Transfer } from 'antd';
import styles from './AllotAuthorize.less';

class UserAllotTransfer extends React.Component {

  state = {
    targetDatas: [],
    targetKeys: [],
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.generateDatas(nextProps);
  }

  generateDatas = (nextProps) => {
    const {authorizeData, allUserList} = nextProps;
    const allotedUserIds = authorizeData.map(item => item.correlateId);
    const targetKeys = [];
    const targetDatas = [];
    for (let i = 0; i < allUserList.length; i++) {
      const item = allUserList[i];
      const data = {
        key: item.id,
        title: item.userName,
        description: item.userName,
        chosen: allotedUserIds && allotedUserIds ? allotedUserIds.indexOf(item.id) > -1 : false,
      };
      if (data.chosen) {
        targetKeys.push(data.key);
      }
      targetDatas.push(data);
    }
    this.setState({ targetDatas, targetKeys });
  };

  filterOption = (inputValue, option) => option.description.indexOf(inputValue) > -1;

  handleChange = targetKeys => {
    this.setState({targetKeys});
  }

  render() {

    const {userAllotTransferVisible, onCancel, onAllotUser} = this.props;
    const { targetDatas, targetKeys } = this.state;

    return (
      <div>
        <Modal
          centered={true}
          visible={userAllotTransferVisible}
          title={"分配用户"}
          onCancel={onCancel}
          onOk={() => onAllotUser(targetKeys)}
          width={550}
          destroyOnClose={false}
          bodyStyle={{height: "350px", padding: "16px"}}
        >
          <Transfer
            listStyle={{height: "320px", width: "280px"}}
            titles={["全部用户", "权限用户"]}
            dataSource={targetDatas}
            showSearch
            filterOption={this.filterOption}
            targetKeys={targetKeys}
            onChange={this.handleChange}
            render={item => item.title}
          />
        </Modal>
      </div>
    );
  };
}

export default UserAllotTransfer;
