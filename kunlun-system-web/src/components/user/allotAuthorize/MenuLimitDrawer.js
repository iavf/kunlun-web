import React from 'react';
import {Form, Input, Row, Col, Tree, Drawer, Button} from 'antd';
import styles from './AllotAuthorize.less';

const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;
const Search = Input.Search;

class MenuLimitDrawer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      checkedTreeNodeKeys: [],
      expandedTreeNodeKeys: []
    };
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const {authorizeData} = nextProps;
    const keys = authorizeData && authorizeData[0] ? (authorizeData[0].authorizeId + ",c4857bf9a7ee45429f42e87699018a86s") : "";
    if (keys && keys.length > 0) {
      this.setState({checkedTreeNodeKeys: keys.split(",")});
    } else {
      this.setState({checkedTreeNodeKeys: []});
    }
  }

  formRef = React.createRef();

  render() {

    const {
      menuLimitDrawerVisible, onClose, menuList, onSelectTreeNode, authorizeData
    } = this.props;
    const { checkedTreeNodeKeys, expandedTreeNodeKeys } = this.state;
    const { getFieldsValue, validateFields, setFieldsValue, resetFields } = this.formRef;

    const formItemLayout = {
      labelCol: {span: 8},
      wrapperCol: {span: 16},
    };

    const onCheckTreeNode = (checkedTreeNodeKeys, item) => {
      this.setState({ checkedTreeNodeKeys })
    }

    const onExpandTreeNode = (expandedTreeNodeKeys, item) => {
      this.setState({ expandedTreeNodeKeys })
    }

    const onOk = () => {
      onSelectTreeNode(checkedTreeNodeKeys);
      this.setState({ checkedTreeNodeKeys: [], expandedTreeNodeKeys: [] });
      onClose();
    }

    const generateTreeNodes = (data) => data.map((item) => {
      if (item.children) {
        return (
          <TreeNode title={item.name} key={item.id} dataRef={item}>
            {generateTreeNodes(item.children)}
          </TreeNode>
        );
      }
      return <TreeNode title={item.name} key={item.id} dataRef={item}/>;
    })

    // 返回工具栏新增、批量删除按钮
    return (
      <div>
        <Drawer
          title={"菜单权限"}
          width={300}
          placement="right"
          onClose={onClose}
          maskClosable={false}
          visible={menuLimitDrawerVisible}
          className={styles.menuDrawer}
        >
          <div className={styles.menuDrawerDiv}>
            <div className={styles.menuDrawerModalDiv}>
              <Form ref={this.formRef}>
                <Row>
                  <Col span={24}>
                    <FormItem {...formItemLayout} name={"level"}>
                      <Search style={{marginLeft: "20px", width: "138%"}} placeholder="请输入菜单名称" onSearch={() => {}} enterButton />
                    </FormItem>
                  </Col>
                </Row>
              </Form>
            </div>
            <div className={styles.menuDrawerTreeDiv}>
              <Tree
                checkable
                autoExpandParent={true}
                onCheck={onCheckTreeNode}
                checkedKeys={checkedTreeNodeKeys}
                expandedKeys={expandedTreeNodeKeys}
                onExpand={onExpandTreeNode}
              >
                {
                  menuList && menuList.length > 0 ? generateTreeNodes(menuList) : null
                }
              </Tree>
            </div>
            <div className={styles.menuDrawerFooterDiv}>
              <Button style={{marginRight: "20px"}} onClick={onClose}>取消</Button>
              <Button type="primary" onClick={onOk}>确定</Button>
            </div>
          </div>
        </Drawer>
      </div>
    );
  };
}

export default MenuLimitDrawer;
