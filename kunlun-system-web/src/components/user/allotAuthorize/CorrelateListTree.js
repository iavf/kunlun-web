import React from 'react';
import {Tree, Row, Radio, Input} from 'antd';
import styles from './AllotAuthorize.less';

const {Search} = Input;

const { TreeNode } = Tree;

const CorrelateListTree = (props) => {

  const {radioValue, correlateList, onSelectTree, onRadioChange, correlateRecord, correlateSelectedKeys} = props;

  const fieldName = "department" == radioValue ? "departmentName" : "post" == radioValue ? "postName" : "roleName";
  const generateTreeNodes = (list) => list.map((item) => {
    if (item.children) {
      return (
        <TreeNode title={item[fieldName]} key={item.id} dataRef={item}>
          {generateTreeNodes(item.children)}
        </TreeNode>
      );
    }
    return <TreeNode title={item[fieldName]} key={item.id} dataRef={item}/>;
  })

  const nameOption = "department" == radioValue ? "部门" : "post" == radioValue ? "岗位" : "角色";

  return (
    <div style={{marginTop: "-2px"}}>
      <Row style={{padding: "0px 0px 12px 0px"}}>
        <Radio.Group onChange={onRadioChange} value={radioValue}>
          <Radio value="department">部门</Radio>
          <Radio value="post">岗位</Radio>
          <Radio value="role">角色</Radio>
        </Radio.Group>
      </Row>
      <Row>
        <Search placeholder={"请输入" + nameOption + "名称"} onSearch={() => {}} enterButton />
      </Row>
      <Row style={{marginLeft: "department" == radioValue ? "-7px" : "-24px", marginTop: "10px"}}>
        <Tree
          className="draggable-tree"
          onSelect={onSelectTree}
          selectedKeys={[correlateRecord && correlateRecord.id]}
          expandedKeys={correlateSelectedKeys}
        >
          {generateTreeNodes(correlateList)}
        </Tree>
      </Row>
    </div>
  );
}

export default CorrelateListTree;
