import * as request from '../../utils/request';
import config from '../../config/config';

export function getLinksList(params) {
  return request.get(`${config.base_api.getLinksList}`, params);
}

export function onSave(params) {
  return request.get(`${config.base_api.onAddLink}`, params);
}
