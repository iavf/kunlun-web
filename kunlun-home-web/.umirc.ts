import { defineConfig } from 'umi';

export default defineConfig({
  dva: {},
  antd: {},
  mfsu: {},
  fastRefresh: {},
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    { path: '/', component: '@/pages/LoginPage' },
    { path: '/kunlun', component: '@/pages/AppPage' },
  ]
});
